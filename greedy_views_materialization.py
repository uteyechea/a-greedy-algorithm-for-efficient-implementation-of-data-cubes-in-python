#!/usr/bin/env python
# coding: utf-8

# In[2]:


import operator


# In[3]:


#The order of the dictionary entries matter because...dependency,etc
lattice={
        'abcd':100,
        'abc':50,'acd':75,
        'ab':20,'ac':30,'cd':40,
        'a':1,'c':10
        }


# In[ ]:


lattice={
    'abcd':
    'abc':,'abd':,'acd':,'bcd':,
    'ab':, 'ac':, 'ad':, 'bc':,'bd':,'cd':,
    
    
    
    
}


# In[4]:


views=list(lattice.keys())
views


# In[5]:


selectedViews=['abcd'] #Always selected by default, also known as Top View


# In[6]:


def dependencyCheck(view,views,lattice=None):
    #compare dependency of view1 with view2
    #if views is a single element compare individual dependency if it is a set give a count 
    #and output a list of dependent views. 
    
    dependentViewsCount=0
    dependentViews=[]
    for anotherView in views:
        n=0
        for dimension in view:
            #print('dimension',dimension)
            for subDimension in anotherView:
                #print('sub dimension',subDimension)
                if dimension == subDimension: 
                    n+=1
                    #print('found, count ', n )
        if n == len(anotherView):
            dependentViews.append(anotherView)
            #print(n,len(anotherView))
            #print('dependent view found',dependentViewsCount)
    return dependentViews


# In[7]:


def additionalBenefit(views,lattice):
    benefit=[]
    minBenefit=0
    for v in views:
        if v not in selectedViews:
            #print(v)
            dependentViewsCount=dependencyCheck(selectedViews[-1],[v])
            if len(dependentViewsCount) <= 0:
                if lattice[v] < lattice[selectedViews[-1]]:
                    
                    addBenefit=abs(lattice[v]-lattice[selectedViews[-1]])
                    benefit.append((addBenefit,v))
                    
    if benefit:
        minBenefit=min(benefit)
        #print(minBenefit,v)
    return minBenefit


# In[8]:


def isConnected(view1,view2):
    isParent=dependencyCheck(view1,[view2],lattice=None)
    if len(isParent) > 0:
        return True
    else:
        return False


# In[9]:


isConnected('abc','abc')


# In[10]:


def getParentView(view,lattice,selectedViews):
    parents=[]
    for v in views:
        if v != view:
            if isConnected(v,view):
                parents.append(v)
    #parents will alwayas have at least the top view
    newParent=views[0] # Top View
    for parent in parents:
            if parent in selectedViews:
                if lattice[parent]<=lattice[newParent]:
                    newParent=parent
    return newParent  


# In[121]:


def getBenefit(views,lattice,selectedViews):
    B={}
    for view in lattice:
        storageCostDifference=0
        benefit=[]
        dependentViews=[]
        if view not in selectedViews:
            parent=getParentView(view,lattice,selectedViews)       
            storageCostDifference=abs(lattice[view]-lattice[parent])
            
            #Count the number of dependent Views
            view_dependentViews=dependencyCheck(view,views)
            
            
            materialized_view_dependentViews=[]
            #for selectedView in selectedViews:    
            #    parent=getParentView(view,lattice,selectedViews)
            #    if parent != selectedView and parent==selectedViews[0]:#Original
            #        materialized_view_dependentViews.extend(dependencyCheck(selectedView,views))
            
            
            for selectedView in selectedViews:
                if selectedView != parent and selectedView!=selectedViews[0]:
                    materialized_view_dependentViews.extend(dependencyCheck(selectedView,views))
                
                
                
                
            dependentViews=            set(view_dependentViews).difference(set(materialized_view_dependentViews))
            if dependentViews==set():
                dependentViews=set(view)
            #estimate the difference only for those selected nodes that are not parents
        
            addBenefit=additionalBenefit(views,lattice)
            if addBenefit and addBenefit[1]== view:  
                benefit.append((view,storageCostDifference,len(dependentViews),                dependentViews,storageCostDifference*len(dependentViews)+addBenefit[0]))
                B[view]=storageCostDifference*len(dependentViews)+addBenefit[0]
            else:
                benefit.append((view,storageCostDifference,len(dependentViews),                dependentViews,storageCostDifference*len(dependentViews)))
                B[view]=storageCostDifference*len(dependentViews)
                
        print(benefit)
        
        #Update Selected Views
        #B[view]=storageCostDifference*len(dependentViews)+addBenefit[0]
        
    update=max(B.items(), key=operator.itemgetter(1))[0]
    selectedViews.append(update)
        
    return selectedViews


# In[122]:


selectedViews=['abcd']
getBenefit(views,lattice,selectedViews)
selectedViews


# In[123]:


getBenefit(views,lattice,selectedViews)
selectedViews


# In[124]:


getBenefit(views,lattice,selectedViews)
selectedViews


# In[120]:


lattice


# In[125]:


getBenefit(views,lattice,selectedViews)
selectedViews


# In[ ]:





# In[ ]:




